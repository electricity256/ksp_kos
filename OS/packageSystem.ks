

print "== package system initializing ==".

declare global PackageSystem is Lexicon().

set PackageSystem["packagesPath"] to "packages".
set PackageSystem["loadedPackages"] to List().
set PackageSystem["packageLoadingStack"] to List().

set PackageSystem["listPackages"] to {
	set currentPath to PATH().
	cd(PackageSystem:packagesPath).
	list files in packageList.
	cd(currentPath).
	return packageList.
}.

set PackageSystem["getPackageFolderPath"] to {
	declare parameter packageName.
	return PackageSystem:packagesPath+"/"+packageName+"/".
}.

set PackageSystem["getPackageRunPath"] to {
	declare parameter packageName.
	return PackageSystem:getPackageFolderPath(packageName)+packageName.
}.

set PackageSystem["loadPackage"] to {
	declare parameter packageName.
	
	if not PackageSystem:packageLoadingStack:indexof(packageName) = -1 { 	// check if currently requested package is already in the process of loading
																			// and throw recursive load error if it is
		set otherPackageName to PackageSystem:packageLoadingStack[PackageSystem:packageLoadingStack:LENGTH-1].
		systemError("Dependency nightmare encountered, packages "+packageName+" and "+otherPackageName+" tried to load each other recursively").
		return.
	}else{
		// add package name to package loading stack, so that recursive loading can be identified
		PackageSystem:packageLoadingStack:add(packageName).
		set currentPath to PATH().
		cd(PackageSystem:getPackageFolderPath(packageName)).
		//print "running path "+PackageSystem:getPackageRunPath(packageName).
		//runoncepath(PackageSystem:getPackageRunPath(packageName)).
		runoncepath(packageName).
		cd(currentPath).
		PackageSystem:packageLoadingStack:remove(PackageSystem:packageLoadingStack:LENGTH-1). // remove it from stack when done loading
		
		// if package that is currently being loaded isnt added to list, add it to loaded pkg list
		if PackageSystem:loadedPackages:indexof(packageName) = -1 {
			PackageSystem:loadedPackages:add(packageName).
		}
	}
}.




























