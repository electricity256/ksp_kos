 

// static function, repeats /identChar/ /count/ amount of times
declare function staticRepeatChar{
	declare parameter count.
	declare parameter identChar.
	
	set leftIdentStr to "".
	FROM {local k is 0.} UNTIL k >= count STEP {set k to k+1.} DO {
		set leftIdentStr to leftIdentStr+identChar.
	}
	
	return leftIdentStr.
}

// static function, calculates how much to ident text from the left
// given text size and screen width
// and returns a string of chars to use as prefix
declare function staticCalcIdentation {
	declare parameter textLength.
	declare parameter screenWidth.
	declare parameter identChar.
	
	set leftIdent to 0.
	if screenWidth>textLength {
		set leftIdent to (screenWidth-textLength)/2.
	}
	//logln("textLength: "+textLength).
	//logln("screenWidth: "+screenWidth).
	//logln("leftIdent:  "+leftIdent).
	return staticRepeatChar(leftIdent,identChar).
}