 


RunOncePath("StaticFuncs").
RunOncePath("Menu").


//declare global MenuSystemClass to class:declare("MenuSystemClass").
//
//set MenuSystemClass["menuStack"] to List().
//set MenuSystemClass["currentMenu"] to Menu:new().
//
//MenuSystemClass:function("constructor",{
//	declare parameter this.
//	set this["menuStack"] to List().
//	set this["currentMenu"] to Menu:new().
//}).
//	
//// will execute previous menu function if added to stack
//// for convenience
//MenuSystemClass:function("previousMenu",{
//	declare parameter this.
//	set len to this:menuStack:length.
//	if len > 1 {
//		this:menuStack:remove(len-1).
//		set obj to this:menuStack[len-2].
//		this:menuStack:remove(len-2).
//		set this["currentMenu"] to obj.
//		obj:initialize().
//	}
//}).
//
//MenuSystemClass:function("addMenu",{
//	declare parameter this.
//	declare parameter menuobject.
//	this:menuStack:add(menuobject).
//}).
//
//MenuSystemClass:function("switchToMenu",{
//	declare parameter this.
//	declare parameter MenuClassParam.
//	
//	//print "1.".
//	//wait 1.
//	//set menuInstanceObj to MenuClassParam.
//	//print "2.".
//	//wait 1.
//	//this:addMenu(menuInstanceObj).
//	//print "3.".
//	//wait 1.
//	//set this["currentMenu"] to menuInstanceObj.
//	//print "4.".
//	//wait 1.
//	//menuInstanceObj:initialize().
//	//print "5.".
//	//wait 1.
//	
//}).
//
//
//
//declare global MenuSystem to MenuSystemClass:new().
//
//MenuSystem:constructor().


//works
declare global MenuSystem to Lexicon(). //class:declare("MenuSystemClass").

set MenuSystem["currentMenu"] to Menu:new().
set MenuSystem["menuStack"] to List().


set MenuSystem["constructor"] to {
	set MenuSystem["menuStack"] to List().
	set MenuSystem["currentMenu"] to Menu:new().
}.
	
// will execute previous menu function if added to stack
// for convenience
set MenuSystem["previousMenu"] to {
	
	set len to MenuSystem:menuStack:length.
	if len > 1 {
		set curMenu to MenuSystem["currentMenu"].
		set obj to MenuSystem:menuStack[len-2].
		
		set curMenu:suppressDraw to true.
		set obj:suppressDraw to false.
	
		MenuSystem:menuStack:remove(len-1).
		//MenuSystem:menuStack:remove(len-2).
		set MenuSystem["currentMenu"] to obj.
		obj:initialize().
		obj:draw().
	}
}.

set MenuSystem["addMenu"] to {
	declare parameter menuobject.
	MenuSystem:menuStack:add(menuobject).
}.

set MenuSystem["switchToMenu"] to {
	declare parameter MenuClassParam.
	
	set menuInstanceObj to MenuClassParam.
	set curMenu to MenuSystem["currentMenu"].
	
	set menuInstanceObj:suppressDraw to false.
	set curMenu:suppressDraw to true.
	
	MenuSystem:addMenu(menuInstanceObj).
	set MenuSystem["currentMenu"] to menuInstanceObj.
	menuInstanceObj:initialize().
	menuInstanceObj:draw().
}.

set MenuSystem["draw"] to {
	MenuSystem:currentMenu:draw().
}.

MenuSystem:constructor().

WHEN terminal:input:haschar() THEN {
	if MenuSystem:currentMenu:captureInput {
		MenuSystem:currentMenu:menuInputEvent(terminal:input:getchar()).
	}else{ // standard controls
		set ch to terminal:input:getchar().
		if ch = terminal:input:DOWNCURSORONE {
			MenuSystem:currentMenu:nextOption().
		}
		if ch = terminal:input:UPCURSORONE {
			MenuSystem:currentMenu:previousOption().
		}
		if ch = terminal:input:RETURN {
			MenuSystem:currentMenu:selectOption().
		}
	}
	return true.
}



