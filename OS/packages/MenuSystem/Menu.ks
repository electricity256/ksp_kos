




declare global Menu to class:declare("Menu").




Menu:function("constructor",{
	declare parameter this.
	
	set this["suppressDraw"] to False.
	
	set this["option"] to 0. // the current option in the list


	set this["optionsList"] to List(). // list of options
	set this["menuWidth"] to 40.		// menu draw width
	set this["logLines"] to 15.			// log list length
	
	
	set this["programName"] to "PISS OS". // program name
	set this["title"] to "menu title".	  // title under program name
	set this["heading"] to "heading".     // heading under title
	
	// temp variables, recalculated only once on an event
	// used for stroring recalculated identation for fast access
	set this["tmpProgramNameIdentation"] to "".
	set this["tmpTitleIdentation"] to "".
	set this["tmpHeadingIdentation"] to "".
	set this["tmpSeparator"] to "".
	
	set this["logLine"] to "== LOG ".
	set this["tmpLogLine"] to "== LOG ".
	
	// gives menu the ability to redirect input for input through the menu	
	set this["captureInput"] to false.
	set this["captureInputBuffer"] to "".
	set this["inputPromptText"] to "input". // text which is to be shown before the input
	
	
	// overridable event - this event is called when
	// user presses enter while this:captureInput is true.
	// submittedText contains the buffer
	set this["menuInputSubmitted"] to {
		declare parameter submittedText.
	}.


	// overridable event - runs when input cancelled
	set this["menuInputCancelled"] to {
		declare parameter submittedText.
	}.
	
	
	set this["logger"] to GlobalLogger. //Logger:new().
	
	
	this:logger:setOnLogModifiedEvent({
		MenuSystem:draw().
	}).
	
}).


Menu:function("initialize",{
	declare parameter this.
	
	set this["programName"] to "PISS OS". // program name
	set this["title"] to "menu title".	  // title under program name
	set this["heading"] to "heading".     // heading under title
	
	
	this:clearOptions().
	this:recalcTemp().

	this:addOption("Option 1",{}).
	this:addOption("Option 2",{}).

}).


Menu:function("setInitializeEvent",{
	declare parameter this.
	declare parameter eventFunc.
	
	
	set this["initialize"] to eventFunc:bind(this).
}).
// input chars are redirected to this function
// when menu:captureInput is true
// clears buffer and disables menu:captureInput if enter pressed
Menu:function("menuInputEvent",{
	declare parameter this.
	declare parameter char.
	//logln("char written "+char).
	if char = terminal:input:BACKSPACE {
		if this:captureInputBuffer:length > 0 {
			set tmp to this:captureInputBuffer:SUBSTRING(0, this:captureInputBuffer:length-1).
			set this:captureInputBuffer to tmp.
		}
		this:draw().
		return.
	}
	if char = terminal:input:ENTER {
		set outp to this:captureInputBuffer.
		set this:captureInputBuffer to "".
		set this:captureInput to false.
		this:menuInputSubmitted(outp).
		return.
	}
	if char = terminal:input:ENDCURSOR { // escape todo
		set outp to this:captureInputBuffer.
		set this:captureInputBuffer to "".
		set this:captureInput to false.
		this:menuInputCancelled(outp).
		return.
	}
	set this:captureInputBuffer to this:captureInputBuffer+char.
	this:draw().
	return.
}).



// function which recalculates identations only when needed
// and stores them in variables for fast use
Menu:function("recalcTemp",{
	declare parameter this.
	//logln("menuWidth: "+this:menuWidth).
	set this:tmpSeparator to staticRepeatChar(this:menuWidth,"=").
	set this:tmpProgramNameIdentation to staticCalcIdentation(this:programName:LENGTH,this:menuWidth," ").
	set this:tmpTitleIdentation to staticCalcIdentation(this:title:LENGTH,this:menuWidth," ").
	set this:tmpHeadingIdentation to staticCalcIdentation(this:heading:LENGTH,this:menuWidth," ").
	set this:tmpLogLine to this:logLine+staticRepeatChar(clamp(this:menuWidth-this:logLine:length,0,this:menuWidth),"=").
	
	//logln("tmpProgramNameIdentation: "+this:tmpProgramNameIdentation:LENGTH).
	//logln("tmpTitleIdentation: "+this:tmpTitleIdentation:LENGTH).
	//logln("tmpHeadingIdentation: "+this:tmpHeadingIdentation:LENGTH).
	//logln("calc: "+clamp(this:menuWidth-this:logLine:length,0,this:menuWidth)).
}).

// adds option in the menu
Menu:function("addOption",{
	declare parameter this.
	declare parameter text.
	declare parameter callback.
	
	declare optionItem to Lexicon().
	set optionItem["text"] to text.
	set optionItem["callback"] to callback@.
	
	this:optionsList:add(optionItem).
}).

// clears all options from the menu
Menu:function("clearOptions",{
	declare parameter this.
	this:optionsList:clear().
}).

// prints the header above the menu
Menu:function("printHeader",{
	declare parameter this.
	PRINT this:tmpSeparator.
	PRINT this:tmpProgramNameIdentation+this:programName.
	PRINT this:tmpTitleIdentation+this:title.
	PRINT this:tmpHeadingIdentation+this:heading.
	PRINT this:tmpSeparator.
}).

// prints all menu options onto the screen, and prefixes
// the current option with a ">" character
Menu:function("printOptions",{
	declare parameter this.
	from {local k is 0.} until k >= this:optionsList:length step {set k to k+1.} do {
		local selectionText is " ".
		if this:option = k {
			set selectionText to ">".
		}
		print selectionText+k+". "+this:optionsList[k]:text.
	}
}).

// clears the screen and draws the whole menu
Menu:function("draw",{
	declare parameter this.
	if not this:suppressDraw {
		if this:captureInput {
			this:drawInputPrompt().
		}else{
			clearscreen.
			this:printHeader().
			PRINT " ".
			this:printOptions().
			PRINT " ".
			PRINT this:tmpLogLine.
			this:logger:printLastLines(this:logLines).
			PRINT this:tmpSeparator.
		}
	}
}).

// clears the screen and draws the whole menu like a text prompt
Menu:function("drawInputPrompt",{
	declare parameter this.
	clearscreen.
	this:printHeader().
	PRINT "Press END to cancel input.".
	PRINT " ".
	PRINT this:inputPromptText+": "+this:captureInputBuffer.
	PRINT " ".
	PRINT this:tmpLogLine.
	this:logger:printLastLines(this:logLines).
	PRINT this:tmpSeparator.
}).

Menu:function("prompt",{
	declare parameter this.
	declare parameter promptText.
	set this:captureInputBuffer to "".
	set this:captureInput 		to true.
	set this:inputPromptText 	to promptText. // text which is to be shown before the input
}).

// overridable event for custom response based on the
// option number that has been passed in as a parameter
Menu:function("onSelected",{
	declare parameter this.
	declare parameter option.
	
	this:optionsList[option]:callback().
}).

// moves the current option to the next option in the list of options
Menu:function("nextOption",{
	declare parameter this.
	if this:option < this:optionsList:length-1 {
		set this:option to this:option+1.
	}else{
		set this:option to 0.
	}
	this:draw().
}).

// moves the current option to the previous option in the list of options
Menu:function("previousOption",{
	declare parameter this.
	if this:option > 0 {
		set this:option to this:option-1.
	}else{
		set this:option to this:optionsList:length-1.
	}
	this:draw().
}).

// selects the current option in the list, thereby also triggering the onSelected event
Menu:function("selectOption",{
	declare parameter this.
	this:onSelected(this:option).
	this:draw().
}).

// sets the title and recalculates idents
Menu:function("setTitle",{
	declare parameter this.
	declare parameter text.
	set this["title"] to text.
	this:recalcTemp().
	this:draw().
}).

// sets the heading and recalculates idents
Menu:function("setHeading",{
	declare parameter this.
	declare parameter text.
	set this["heading"] to text.
	this:recalcTemp().
	this:draw().
}).

