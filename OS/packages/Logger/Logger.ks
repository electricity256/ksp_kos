 

declare global Logger to class:declare("Logger").




Logger:function("constructor",{
	declare parameter this.
	
	set this["list"] to List().
	this:list:add("").

	set this["identation"] to "  ".
	
	
	set this["onLogModified"] to {
		
	}.
	
}).

	
Logger:function("_log",{
	declare parameter this.
	declare parameter text.
	set listid to (this:list:LENGTH-1).
	set tmp1 to this:list[listid].
	set tmp1 to tmp1+text.
	this:list:remove(listid).
	this:list:insert(listid,tmp1).
}).
	
Logger:function("logf",{
	declare parameter this.
	declare parameter text.
	this:_log(text).
	this:callOnLogModifiedEvent().
}).

Logger:function("logLn",{
	declare parameter this.
	declare parameter text.
	this:_log(text).
	this:list:add("").
	this:callOnLogModifiedEvent().
}).

Logger:function("clearLog",{
	declare parameter this.
	this:list:clear().
	this:list:add("").
	this:callOnLogModifiedEvent().
}).

Logger:function("callOnLogModifiedEvent",{
	declare parameter this.
	
	if this:haskey("onLogModified") {
		this:onLogModified().
	}
	
}).
	
Logger:function("printLastLines",{
	declare parameter this.
	declare parameter count.
	set listlen to this:list:length.
	if count > listlen {
		set count to listlen.
	}
	set startid to listlen-count.
	FROM {local k is startid.} UNTIL k >= listlen STEP {set k to k+1.} DO {
		PRINT this:identation + (this:list)[k].
	}
}).
	
Logger:function("setOnLogModifiedEvent",{
	declare parameter this.
	declare parameter event.
	set this["onLogModified"] to event@.
}).



declare global GlobalLogger to Logger:new().

set logf@ to { // meant to be overridable
	declare parameter text.
	GlobalLogger:logf(text).
}.

set logln@ to { // meant to be overridable
	declare parameter text.
	GlobalLogger:logln(text).
}.