 

declare global DecoupleService to Service:new().
set DecoupleService["autorun"] to True.
set DecoupleService["name"] to "DecoupleService".

set DecoupleService["interval"] to 1.0.
set DecoupleService["decoupleStage"] to 0.
set DecoupleService["futureTime"] to 0.




DecoupleService:setOnStartedEvent({
	declare parameter this.

	logln("(DecoupleService) starting service.").
	set this["interval"] to 1.0.
	set this["decoupleStage"] to 0.
	set this["futureTime"] to TIME:SECONDS + this:interval.
	
	
}).
//
DecoupleService:setOnStoppedEvent({
	declare parameter this.
	
	logf("(DecoupleService) Stopping Service...").
	set this:decoupleStage to 100.
	
}).
//
//


WHEN TIME:SECONDS > DecoupleService:futureTime AND DecoupleService["activated"] THEN {
	SET DecoupleService:futureTime to TIME:SECONDS + DecoupleService:interval.
	//logln("DS running.").
	////////////////////////////////////////////////////////////////////////////////////
	if DecoupleService:decoupleStage = 0 {
			set tanks to ship:partsTagged("attachedtank").
			for tank in tanks {
				if tank:Resources[0]:amount <= 0 {
					logf("(DecoupleService) De-coupling empty booster...").
					tank:parent:getmodule("ModuleAnchoredDecoupler"):doaction("decouple", true).
					logln("OK!").
				}
			}
			if tanks:length = 0 {
			logln("(DecoupleService) All boosters de-coupled.").
			set DecoupleService:decoupleStage to 1.
		}
	}

	if DecoupleService:decoupleStage = 1 {
		set tmp to ship:partsTagged("bottomtank").
		if tmp:length = 0 {
			logln("(DecoupleService) WARNING - did not find ship's bottom tanks...").
			set DecoupleService:decoupleStage to 2.
			return true.
		}
		set bottomtank to tmp[0].
		if bottomtank:Resources[0]:amount <=0 {

			set tmp to ship:partsTagged("decoupler2").
			if tmp:length = 0 {
				logln("(DecoupleService) WARNING - did not find ship's 2nd decoupler...").
				set DecoupleService:decoupleStage to 2.
				return true.
			}
			set decoupler to tmp[0].

			set tmp to ship:partsTagged("engine2").
			if tmp:length = 0 {
				logln("(DecoupleService) WARNING - did not find ship's 2nd engine...").
				set DecoupleService:decoupleStage to 2.
				return true.
			}
			set engine2 to tmp[0].

			logf("(DecoupleService) De-coupling empty fuel tank...").
			decoupler:getmodule("ModuleDecouple"):doaction("decouple", true).
			logln("OK!").
			logf("(DecoupleService) Activating second engine...").
			engine2:activate().
			logln("OK!").
			set DecoupleService:decoupleStage to 2.
		}
	}
	
	if DecoupleService:decoupleStage = 100 {
		logf("(DecoupleService) Stopped.").
	}
	return true.
	
}

ServiceSystem:registerService(DecoupleService,"DecoupleService").



