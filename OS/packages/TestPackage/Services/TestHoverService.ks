
// gravity kerbin 9.81 m/s (or 10 or 9.85)
// thrust max: 15.2 kN*4
// 100% thrust = 60.8 kN
// ship:mass : 1.84 ?
// mass * acceleration = force
// ship:mass * -ship:velocity:z = force
// force / 60.8 = burn time
// ship:velocity:surface   // Math.sqrt(Math.pow(-6.56,2)+Math.pow(-4.46,2)+Math.pow(8.125,2))
//set burntime to (ship:mass*-ship:velocity:surface:z)/60.8.

// hover thrust
// % throttle = (constant:g0 / 60.8 )*100

//set throttle to (constant:g0 * ship:mass) / 60.8.
//set initialrotation to ship:facing.
//lock steering to initialrotation.
//Up+R(0,-90,270).


declare global hoverControlYaw to 0.
declare global hoverControlPitch to 0.

declare function _getEngines {
	set engineName to "smallRadialEngine.v2".
	set retn to List().
	for part in ship:parts {
		if part:name = engineName {
			retn:add(part).
		}
	}
	return retn.
}

declare function _activateEngine {
	declare parameter engine.
	engine:getmodule("ModuleEnginesFX"):doaction("activate engine", true).
}

declare function _activateEngines {
	set enginelist to _getEngines().
	for engine in enginelist {
		_activateEngine(engine).
	}
}



//declare function _hover {
//	wait 1.
//	set isReady to true.
//}





declare global TestHoverService to Service:new().
set TestHoverService["autorun"] to False.
set TestHoverService["name"] to "TestHoverService".

set TestHoverService["interval"] to 1.0.
set TestHoverService["futureTime"] to 0.
set TestHoverService["isReady"] to false.




TestHoverService:setOnStartedEvent({
	declare parameter this.
	
	logln("("+this:name+") starting service.").
	set this["interval"] to 1.0.
	set this["futureTime"] to TIME:SECONDS + this:interval.
	set this:isReady to false.
	
	SAS on.
	_activateEngines().
	lock throttle to 1.
	
	//_hover().
	
	
}).
//
TestHoverService:setOnStoppedEvent({
	declare parameter this.
	
	unlock throttle.
	unlock steering.
	SAS on.
	logf("("+this:name+") Stopping Service...").
	set this:isReady to false.
	
}).
//
//


when TestHoverService:futureTime<TIME:SECONDS AND (not TestHoverService:isReady) AND TestHoverService["activated"] then {
	SAS off.
	set TestHoverService:isReady to true.
	lock steering to Up+R(0+hoverControlYaw,-90+hoverControlPitch,270).
	return true.
}

//ship:velocity:surface:z <1 AND 
when TestHoverService:isReady AND TestHoverService["activated"] then {
	// F (newtons) = (Gravity constant * planet (weight) * ship (weight) ) / (hight from surface (meters) + planet radius (meters))^2
	set grav1 to (constant:G * Body("Kerbin"):mass * ship:mass) / (altitude()+Body("Kerbin"):radius)^2.
	// 1 engine effeciency
	// asl: 275
	// vac: 290
	// ratio: 0.948275862069
	// difference: 0.051724137931
	// 
	set myalt to altitude().
	set atmosp to exp( -myalt / 5600.0 ).
	set engineEffeciencyRatio to (0.948275862069 + (0.051724137931 * atmosp)).
	set adjustedMaxNewtons to (60.8 / engineEffeciencyRatio).
	
	lock throttle to (grav1 / adjustedMaxNewtons).	
	//((grav1) * ship:mass) / (60.8).	
	
	// calcumalate the engin effeciency
	// 60.8 is the total engines thrust
	// 0.948275862069 is the engines' effeciency ratio
	// 0.051724137931 is 1 - engines effeciency ratio
	// 20000 is the altitude
	// 5600 is kerbin's scale hight - The scale height of an atmosphere define at which rate the pressure drops with altitude.
	//60.8 / (0.948275862069 + (0.051724137931 * exp(-20000/5600)))
	
	//-ship:velocity:surface:z
	//logf(";"+ship:velocity:surface:z).
	//logf(";"+throttle).
	
	return true.
}

//when ship:velocity:surface:z >=0 AND TestHoverService:isReady AND TestHoverService["activated"] then {
//	lock throttle to 0.
//	return true.
//}


ServiceSystem:registerService(TestHoverService,"TestHoverService").

//print (constant:G * Body("Kerbin"):mass * ship:mass) / (ship:altitude+Body("Kerbin"):radius)^2.