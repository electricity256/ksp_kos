 
 
 
declare global ServiceManagerMenu_ServiceOptions to Menu:new().
set DummyService to Service:new().
set ServiceManagerMenu_ServiceOptions["selectedService"] to DummyService.

ServiceManagerMenu_ServiceOptions:setInitializeEvent({
	declare parameter this.
	
	if this["selectedService"] = DummyService {
		set this:title to "Service Manager - None".
		set this:heading to "Invalid service selected".
	} else {
		set this:title to ("Service Manager - "+this["selectedService"]:name).
		if this["selectedService"]:activated {
			set this:heading to "service is going brr...".
		} else {
			set this:heading to "service is stopped".
		}
	}
    
	this:clearOptions().
	this:recalcTemp().
	
	
	this:addOption("Start",{
		if ServiceManagerMenu_ServiceOptions["selectedService"] = DummyService {
			logln("Failed to start service, none selected").
		} else {
			ServiceManagerMenu_ServiceOptions["selectedService"]:start().
		}
	}).
	//
	//
	this:addOption("Stop",{
		if ServiceManagerMenu_ServiceOptions["selectedService"] = DummyService {
			logln("Failed to stop service, none selected").
		} else {
			ServiceManagerMenu_ServiceOptions["selectedService"]:stop().
		}
	}).
	
	
	this:addOption("Back",{
		MenuSystem:previousMenu().
	}).
	
	
}).


