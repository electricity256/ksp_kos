 

declare global AutoExpandOrbitMenu to Menu:new().
declare global AutoExpandOrbitMenu_SetAltitude to Menu:new().
declare global AutoExpandOrbitMenu_SetCheckpoint to Menu:new().
declare global AutoExpandOrbitMenu_Started to Menu:new().


set autoOrbitExpandAlt to 0.
set autoOrbitExpandCheckpoint to "apoapsis". // "apoapsis" or "nextnode" or "now"


AutoExpandOrbitMenu:setInitializeEvent({
	declare parameter this.
	
	set this:title to "Auto Orbit Expand".
	set this:heading to "configuration".
	
	this:clearOptions().
	this:recalcTemp().
	
	this:addOption("Set altitude (cur: "+autoOrbitExpandAlt+")",{
		MenuSystem:switchToMenu(AutoExpandOrbitMenu_SetAltitude).
	}).
	
	this:addOption("Set checkpoint (cur: "+autoOrbitExpandCheckpoint+")",{
		MenuSystem:switchToMenu(AutoExpandOrbitMenu_SetCheckpoint).
	}).
	
	this:addOption("Start sequence",{
		MenuSystem:switchToMenu(AutoExpandOrbitMenu_Started).
	}).
	
	this:addOption("Back",{
		MenuSystem:previousMenu().
	}).
		
}).


AutoExpandOrbitMenu_SetAltitude:setInitializeEvent({
	declare parameter this.
	
	set this:title to "Auto Orbit Expand".
	set this:heading to "set target altitude".
	
	this:clearOptions().
	this:recalcTemp().
	
	this:prompt("target altitude").
	
	set this["menuInputSubmitted"] to {
		declare parameter submittedText.
		set val to submittedText:toNumber(-1).
		if val = -1 {
			logln("Value "+submittedText+" is not a number.").
			logln("Target altitude was not modified.").
			MenuSystem:previousMenu().
			return.
		}
		if val <= 0 {
			logln("Specified value must be greater than 0").
			logln("Target altitude was not modified.").
			MenuSystem:previousMenu().
			return.
		}
		set autoOrbitExpandAlt to val.
		//logln("You entered input: "+submittedText).
		MenuSystem:previousMenu().
	}.
	
	set this["menuInputCancelled"] to {
		declare parameter submittedText.
		logln("Target altitude was not modified.").
		MenuSystem:previousMenu().
	}.
	
}).


AutoExpandOrbitMenu_SetCheckpoint:setInitializeEvent({
	declare parameter this.
	
	set this:title to "Auto Orbit Expand".
	set this:heading to "set starting checkpoint".
  
	this:clearOptions().
	this:recalcTemp().

	this:addOption("Apoapsis",{
		set autoOrbitExpandCheckpoint to "apoapsis".
		MenuSystem:previousMenu().
	}).
	this:addOption("Next Node",{
		set autoOrbitExpandCheckpoint to "nextnode".
		MenuSystem:previousMenu().
	}).
	this:addOption("Current Position",{
		set autoOrbitExpandCheckpoint to "now".
		MenuSystem:previousMenu().
	}).
	this:addOption("Back",{
		MenuSystem:previousMenu().
	}).
	
}).



AutoExpandOrbitMenu_Started:setInitializeEvent({
	declare parameter this.
	
	set this:title   to "Auto Orbit Expand".
	set this:heading to "starting...".
	
	this:clearOptions().
	this:recalcTemp().

	this:addOption("Stop & Back",{
		MenuSystem:previousMenu().
	}).

	set additionalTimeFromNow to 0.
	
	if autoOrbitExpandCheckpoint = "apoapsis" {
		set additionalTimeFromNow to ETA:Apoapsis-10.
	}
	if autoOrbitExpandCheckpoint = "nextnode" {
		if HasNode {
			set additionalTimeFromNow to nextnode:ETA-10.
		}else{
			logln("Error: you've not set a node").
			MenuSystem:previousMenu().
		}
	}
	if autoOrbitExpandCheckpoint = "now" {
		set additionalTimeFromNow to 10.
	}
	
	

	set timerMode to 0.
	set interval to 1.0.
	set targetStartTime to additionalTimeFromNow+TIME:SECONDS.
	
	logf("! Turning off SAS and RCS...").
	RCS off.
	SAS off.
	logln("OK!").
	
	set modeAutomaticExpandOrbitMenu_Started_running to true.
  
	SET futureTime to TIME:SECONDS + interval.
	WHEN TIME:SECONDS > futureTime THEN {
		SET futureTime to TIME:SECONDS + interval.
		
		lock steering to ship:velocity:orbit.
		
		if timerMode = 0 {
			this:setHeading("Idle until target "+(targetStartTime-TIME:SECONDS)+"s").
			if TIME:SECONDS > targetStartTime {
				logf("! Expanding periapsis to match apoapsis...").
				this:setHeading("Expanding periapsis...").
				set interval to 0.008.
				set ship:control:pilotMainThrottle to 1.
				set timerMode to 1.
			}
			return modeAutomaticExpandOrbitMenu_Started_running.
		}
		
		if timerMode = 1 {
			// (alt:periapsis + 1000) > altitude()
			if ALT:APOAPSIS > autoOrbitExpandAlt {
				set interval to 1.
				set ship:control:pilotMainThrottle to 0.
				logln("OK!").
				
				set targetStartTime to TIME:SECONDS+ETA:APOAPSIS-10.
				logf("! Waiting to reach apoapsis...").
				this:setHeading("Idle until apoapsis").
				set timerMode to 2.
			}
			return modeAutomaticExpandOrbitMenu_Started_running.
		}
		
		if timerMode = 2 {
			this:setHeading("Idle until target "+(targetStartTime-TIME:SECONDS)+"s").
			if TIME:SECONDS > targetStartTime {
				logln("OK!").
				
				set interval to 0.008.
				set ship:control:pilotMainThrottle to 1.
				logf("! Expanding periapsis to match target alt...").
				set timerMode to 3.
			}
			return modeAutomaticExpandOrbitMenu_Started_running.
		}
		
		if timerMode = 3 {
			if (alt:periapsis + 1000) > altitude() {
				set interval to 1.
				set ship:control:pilotMainThrottle to 0.
				logln("OK!").
				
				logf("! Unlocking steering...").
				unlock steering.
				logln("OK!").
				
					
				logf("! Turning on SAS and RCS...").
				RCS on.
				SAS on.
				logln("OK!").

				set modeAutomaticExpandOrbitMenu_Started_running to false.
				logln("= sequence completed =").
				this:setHeading("Finished").
			}
			return modeAutomaticExpandOrbitMenu_Started_running.
		}
		
		return modeAutomaticExpandOrbitMenu_Started_running.
	}
	




}).

