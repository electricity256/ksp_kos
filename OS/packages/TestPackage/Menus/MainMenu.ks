
declare global MainMenu to Menu:new().

MainMenu:setInitializeEvent({
	declare parameter this.
	
	
	set this["title"] to "main menu".	  // title under program name
	set this["heading"] to "idle".     // heading under title

	
	
	this:clearOptions().
	this:recalcTemp().


	this:addOption("Auto orbit",{
		MenuSystem:switchToMenu(AutoOrbitMenu).
	}).

	this:addOption("Auto expand orbit",{
		MenuSystem:switchToMenu(AutoExpandOrbitMenu).
	}).

	this:addOption("Auto shrink orbit",{
		//modeAutomaticShrinkOrbitMenu().
		
	}).

	this:addOption("Manual tasks",{
		//modeManualMenu().
		MenuSystem:switchToMenu(ManualControlMenu).
		
	}).

	this:addOption("Tests",{
		//modeMenuTest().
		MenuSystem:switchToMenu(TestMenu).
		
	}).

	this:addOption("Service Manager",{
		//modeMenuTest().
		MenuSystem:switchToMenu(ServiceManagerMenu).
		
	}).
	
	

	this:addOption("Reboot",{
		clearscreen.
		set exit to true.
		reboot.
	}).
	

}).
