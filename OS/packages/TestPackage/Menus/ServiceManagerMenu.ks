 
 
declare global ServiceManagerMenu to Menu:new().

ServiceManagerMenu:setInitializeEvent({
	declare parameter this.
	
	set this:title to "Service Manager".
	set this:heading to "services going brr...".
    
	this:clearOptions().
	this:recalcTemp().
	
	for k in ServiceSystem:registeredServices:keys {
		set svc to ServiceSystem:registeredServices[k].
		set svcName to svc:name.
		
		set svcStatus to "Stopped".
		if svc:activated {
			set svcStatus to "Started".
		}
		
		this:addOption((svcName +" ("+svcStatus+")"),{
			set ServiceManagerMenu_ServiceOptions["selectedService"] to svc.
			MenuSystem:switchToMenu(ServiceManagerMenu_ServiceOptions).
		}).
	}
	
	this:addOption("Back",{
		MenuSystem:previousMenu().
	}).
	
}).


