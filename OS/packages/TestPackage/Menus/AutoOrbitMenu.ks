 

declare global AutoOrbitMenu to Menu:new().

AutoOrbitMenu:setInitializeEvent({
	declare parameter this.
	
	
	set this["title"] to "Auto Orbit Seq".	  // title under program name
	set this["heading"] to "pending...".     // heading under title


	this:clearOptions().
	this:recalcTemp().

	set this["timerMode"] to 0.
	//set this["decoupleStage"] to 0.
	set this["interval"] to 1.0.

	this:addOption("Stop & Back",{
		MenuSystem:previousMenu().
		set this:timerMode to 100.
	}).
	
	

	launchShip().


	SET futureTime to TIME:SECONDS + this:interval.
	WHEN TIME:SECONDS > futureTime THEN {
		SET futureTime to TIME:SECONDS + this:interval.

		////////////////////////////////////////////////////////////////////////////////////
		//if this:decoupleStage = 0 {
		//		set tanks to ship:partsTagged("attachedtank").
		//		for tank in tanks {
		//			if tank:Resources[0]:amount <= 0 {
		//				this:logger:logf("! De-coupling empty booster...").
		//				tank:parent:getmodule("ModuleAnchoredDecoupler"):doaction("decouple", true).
		//				this:logger:logln("OK!").
		//			}
		//		}
		//		if tanks:length = 0 {
		//		this:logger:logln("! All boosters de-coupled.").
		//		set this:decoupleStage to 1.
		//	}
		//}
        //
		//if this:decoupleStage = 1 {
		//	set tmp to ship:partsTagged("bottomtank").
		//	if tmp:length = 0 {
		//		this:logger:logln("! WARNING - did not find ship's bottom tanks...").
		//		set this:decoupleStage to 2.
		//		return true.
		//	}
		//	set bottomtank to tmp[0].
		//	if bottomtank:Resources[0]:amount <=0 {
        //
		//		set tmp to ship:partsTagged("decoupler2").
		//		if tmp:length = 0 {
		//			this:logger:logln("! WARNING - did not find ship's 2nd decoupler...").
		//			set this:decoupleStage to 2.
		//			return true.
		//		}
		//		set decoupler to tmp[0].
        //
		//		set tmp to ship:partsTagged("engine2").
		//		if tmp:length = 0 {
		//			this:logger:logln("! WARNING - did not find ship's 2nd engine...").
		//			set this:decoupleStage to 2.
		//			return true.
		//		}
		//		set engine2 to tmp[0].
        //
		//		this:logger:logf("! De-coupling empty fuel tank...").
		//		decoupler:getmodule("ModuleDecouple"):doaction("decouple", true).
		//		this:logger:logln("OK!").
		//		this:logger:logf("! Activating second engine...").
		//		engine2:activate().
		//		this:logger:logln("OK!").
		//		set this:decoupleStage to 2.
		//	}
		//}

		/////////////////////////////////////////////////////////////////////////////////////
		if this:timerMode = 0 {                                  // launch sequence
			this:setHeading("Launching...").
			this:logger:logln("! Starting steering sequence").
			set this:timerMode to 1.
			return true.
		}

		if this:timerMode = 1 {                                  // steering sequence
			set height to altitude().
			if height<20000 {
				set ang to R(0,0,0).
			}
			if height>20000 and height<40000 {
				set calcumalation to (height-20000.0)/20000.0.
				if calcumalation > 1.0 {
					set calcumalation to 1.0.
				}
				set ang to R(0,-arcsin(calcumalation),0).
			}
			if height>40000 and height<70000 {
				set ang to R(0,-90,0).
			}
			if height>70000 {
				this:logger:logln("! Steering Sequence finished.").
				this:logger:logln("! Preparing to expand periapsis...").
				set ship:control:pilotMainThrottle to 0.
				this:setHeading("Idle until apoapsis").
				set this:timerMode to 2.
			}
			LOCK STEERING TO Up + ang.
			return true.
		}

		if this:timerMode = 2 {                                // half orbit reached
			if ETA:Apoapsis < 8 {                           // wait to reach apoapsis
				this:logger:logf("! Expanding periapsis to match apoapsis...").
				set ship:control:pilotMainThrottle to 1.
				set this:interval to 0.08.
				this:setHeading("Expanding periapsis...").
				set this:timerMode to 3.
			}
			return true.
		}


		if this:timerMode = 3 {                                // expand periapsis
			if (alt:periapsis + 1000) > altitude() {      // check if periapsis reached apoapsis
				set ship:control:pilotMainThrottle to 0.
				this:logger:logln("OK!").
				set this:interval to 1.0.
				set this:timerMode to 4.
			} 
			return true.
		}

		if this:timerMode = 4 {
			this:logger:logf("! Unlocking steering...").
			unlock steering.
			this:logger:logln("OK!").
			this:logger:logf("! Turning on SAS and RCS...").
			RCS on.
			SAS on.
			this:logger:logln("OK!").
			this:logger:logln("= sequence completed =").
			this:setHeading("Finished").
			return false.
		}
		
		if this:timerMode = 100 {
			this:logger:logln("= sequence interrupted =").
			this:logger:logf("! Unlocking steering...").
			unlock steering.
			this:logger:logln("OK!").
			this:logger:logf("! Turning on SAS and RCS...").
			RCS on.
			SAS on.
			this:logger:logln("OK!").
			return false.
		}
		return true.
	}
	
}).
