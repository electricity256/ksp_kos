 

declare global TestMenu to Menu:new().
declare global MenuPromptTest to Menu:new().



TestMenu:setInitializeEvent({
	declare parameter this.
	
	
	set this["title"] to "Test Menu".	  // title under program name
	set this["heading"] to "idle".     // heading under title


	this:clearOptions().
	this:recalcTemp().


	this:addOption("Test prompt",{
		MenuSystem:switchToMenu(MenuPromptTest).
	}).

	this:addOption("Back",{
		MenuSystem:previousMenu().
	}).
}).
 


MenuPromptTest:setInitializeEvent({
	declare parameter this.
	
	
	set this["title"] to "Prompt Menu".	  // title under program name
	set this["heading"] to "test".     // heading under title


	this:clearOptions().
	this:recalcTemp().
  
	this:prompt("enter input").

	
	set this["menuInputSubmitted"] to {
		declare parameter submittedText.
		this:logger:logln("You entered input: "+submittedText).
		MenuSystem:previousMenu().
	}.
	
	set this["menuInputCancelled"] to {
		declare parameter submittedText.
		this:logger:logln("You've cancelled the prompt").
		MenuSystem:previousMenu().
	}.

	
}).

