 

declare global HoverControlMenu to Menu:new().
declare global HoverControlMenu_setYaw to Menu:new().
declare global HoverControlMenu_setPitch to Menu:new().





HoverControlMenu_setYaw:setInitializeEvent({
	declare parameter this.
	
	set this:title to "Hover Control".
	set this:heading to "set yaw".
	
	this:clearOptions().
	this:recalcTemp().
	
	this:prompt("yaw angle").
	
	set this["menuInputSubmitted"] to {
		declare parameter submittedText.
		set val to submittedText:toNumber(-1).
		if val = -1 {
			logln("Value "+submittedText+" is not a number.").
			logln("Target yaw was not modified.").
			MenuSystem:previousMenu().
			return.
		}
		set hoverControlYaw to val.
		//logln("You entered input: "+submittedText).
		MenuSystem:previousMenu().
	}.
	
	set this["menuInputCancelled"] to {
		declare parameter submittedText.
		logln("Target yaw was not modified.").
		MenuSystem:previousMenu().
	}.
}).

HoverControlMenu_setPitch:setInitializeEvent({
	declare parameter this.
	
	set this:title to "Hover Control".
	set this:heading to "set pitch".
	
	this:clearOptions().
	this:recalcTemp().
	
	this:prompt("target pitch").
	
	set this["menuInputSubmitted"] to {
		declare parameter submittedText.
		set val to submittedText:toNumber(-300000).
		if val = -300000 {
			logln("Value "+submittedText+" is not a number.").
			logln("Target pitch was not modified.").
			MenuSystem:previousMenu().
			return.
		}
		set hoverControlPitch to val.
		//logln("You entered input: "+submittedText).
		MenuSystem:previousMenu().
	}.
	
	set this["menuInputCancelled"] to {
		declare parameter submittedText.
		logln("Target pitch was not modified.").
		MenuSystem:previousMenu().
	}.
}).

HoverControlMenu:setInitializeEvent({
	declare parameter this.
	
	
	set this["title"] to "Hover Control".	  // title under program name
	set this["heading"] to "-".     // heading under title

	
	
	this:clearOptions().
	this:recalcTemp().

	set textToggled to "off".
	if TestHoverService["activated"] {
		set textToggled to "on".
	}

	this:addOption("Toggle hover ("+textToggled+")",{
		if TestHoverService["activated"] {
			ServiceSystem:stopService("TestHoverService").
		} else {
			ServiceSystem:startService("TestHoverService").
		}
		MenuSystem:draw().
		//MenuSystem:switchToMenu(HoverControlMenu).
	}).

	this:addOption("Set Yaw ("+hoverControlYaw+")",{
		MenuSystem:switchToMenu(HoverControlMenu_setYaw).
	}).

	this:addOption("Set Pitch ("+hoverControlPitch+")",{
		MenuSystem:switchToMenu(HoverControlMenu_setPitch).
	}).
	
	this:addOption("Back",{
		MenuSystem:previousMenu().
	}).
	

}).
