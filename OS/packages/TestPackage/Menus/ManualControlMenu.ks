 
declare global ManualControlMenu to Menu:new().

ManualControlMenu:setInitializeEvent({
	declare parameter this.
	
	set this:title to "Manual Control".
	set this:heading to "pending...".

	this:clearOptions().
	this:recalcTemp().
	
	this:addOption("Launch",{
		launchShip().
	}).
	this:addOption("Hover Control",{
		MenuSystem:switchToMenu(HoverControlMenu).
	}).
	this:addOption("Detach Boosters",{
		detachDecouplers("Hydraulic Detachment Manifold","ModuleAnchoredDecoupler","decouple").
	}).
	this:addOption("Decouple 1",{
		detachDecouplersTagged("decoupler1","ModuleDecouple","decouple").
		activateTaggedEngines("engine2").
	}).
	this:addOption("Shit.",{
		detachDecouplersTagged("parachute","ModuleParachute","deploy chute").
	}).
	this:addOption("Debug info",{
		this:logger:logln("here is debug info: ").
		printDebugInfo().
	}).
	this:addOption("Back",{
		MenuSystem:previousMenu().
	}).
	
}).


