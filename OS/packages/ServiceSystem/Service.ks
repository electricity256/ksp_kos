
declare global Service to class:declare("Service").

set Service["activated"] to False.
set Service["autorun"] to False.
set Service["name"] to "UnnamedService".


Service:function("constructor",{
	declare parameter this.
	set this["activated"] to False.
	set this["autorun"] to False.
	set this["name"] to "UnnamedService".
	
}).

Service:function("start",{
	declare parameter this.
	if not this["activated"] {
		this:onStarted().
		set this["activated"] to True.
	}
}).

Service:function("stop",{
	declare parameter this.
	if this["activated"] {
		this:onStopped().
		set this["activated"] to False.
	}
}).


Service:function("onStarted",{
	declare parameter this.
	
}).

Service:function("onStopped",{
	declare parameter this.
	
}).

Service:function("isActive",{
	declare parameter this.
	return this["activated"].
}).

Service:function("setOnStartedEvent",{
	declare parameter this.
	declare parameter eventFunc.
	
	set this["onStarted"] to eventFunc:bind(this).
	
}).

Service:function("setOnStoppedEvent",{
	declare parameter this.
	declare parameter eventFunc.
	
	set this["onStopped"] to eventFunc:bind(this).
	
}).




