 
 

RunOncePath("Service").



//declare global ServiceSystemClass to class:declare("ServiceSystemClass").
//
//ServiceSystemClass:function("constructor",{
//	declare parameter this.
//	set this["registeredServices"] to Lexicon().
//	
//}).
//
//ServiceSystemClass:function("registerService",{
//	declare parameter this.
//	declare parameter serviceObj.
//	declare parameter serviceName.
//	if not this:registeredServices:HASSUFFIX(serviceName) {
//		set this:registeredServices[serviceName] to serviceObj.
//	} else {
//		systemError("service '"+serviceName+"' is already registered.").
//	}
//	
//}).
//
//ServiceSystemClass:function("startService",{
//	declare parameter this.
//	declare parameter serviceName.
//	if this:registeredServices:HASSUFFIX(serviceName) {
//		this:registeredServices[serviceName]:start().
//	} else {
//		systemError("service '"+serviceName+"' does not exist.").
//	}
//}).
//
//ServiceSystemClass:function("stopService",{
//	declare parameter this.
//	declare parameter serviceName.
//	if this:registeredServices:HASSUFFIX(serviceName) {
//		this:registeredServices[serviceName]:stop().
//	} else {
//		systemError("service '"+serviceName+"' does not exist.").
//	}
//}).
//
//ServiceSystemClass:function("restartService",{
//	declare parameter this.
//	declare parameter serviceName.
//	if this:registeredServices:HASSUFFIX(serviceName) {
//		this:stopService(serviceName).
//		this:startService(serviceName).
//	} else {
//		systemError("service '"+serviceName+"' does not exist.").
//	}
//}).
//
//
//declare global ServiceSystem to ServiceSystemClass:new().

////



declare global ServiceSystem to Lexicon().

set ServiceSystem["registeredServices"] to Lexicon().


set ServiceSystem["constructor"] to {
	set ServiceSystem["registeredServices"] to Lexicon().
	
}.

set ServiceSystem["registerService"] to {
	declare parameter serviceObj.
	declare parameter serviceName.
	
	if not ServiceSystem:registeredServices:HASKEY(serviceName) {
		set tmpLex to ServiceSystem:registeredServices.
		set tmpLex[serviceName] to serviceObj.
		if serviceObj:autorun {
			serviceObj:start().
		}
	} else {
		systemError("service '"+serviceName+"' is already registered.").
	}
	
}.

set ServiceSystem["startService"] to {
	declare parameter serviceName.
	if ServiceSystem:registeredServices:HASKEY(serviceName) {
		ServiceSystem:registeredServices[serviceName]:start().
	} else {
		systemError("service '"+serviceName+"' does not exist.").
	}
}.

set ServiceSystem["stopService"] to {
	declare parameter serviceName.
	if ServiceSystem:registeredServices:HASKEY(serviceName) {
		ServiceSystem:registeredServices[serviceName]:stop().
	} else {
		systemError("service '"+serviceName+"' does not exist.").
	}
}.

set ServiceSystem["restartService"] to {
	declare parameter serviceName.
	if ServiceSystem:registeredServices:HASKEY(serviceName) {
		ServiceSystem:stopService(serviceName).
		ServiceSystem:startService(serviceName).
	} else {
		systemError("service '"+serviceName+"' does not exist.").
	}
}.



