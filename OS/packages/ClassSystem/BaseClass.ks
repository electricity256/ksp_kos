

declare global BaseClass is Lexicon().

set BaseClass["functionList"] to List().
set BaseClass["superclassesList"] to List().
set BaseClass["type"] to "BaseClass".

set BaseClass["function"] to {
	declare parameter thisClass.
	declare parameter functionName.
	declare parameter functionDelegate.
	
	set funcId to thisClass:functionList:find(functionName).
	
	if funcId = -1 {
		thisClass:functionList:add(functionName).
		set thisClass[functionName] to functionDelegate@.
	}else{
		systemError("Attempted to declare an existing function '"+functionName+"' in class '"+thisClass:type+"'.").
	}
	
}.

set BaseClass["new"] to {
	declare parameter thisClass.
	return class:new(thisClass:type).
}.
