
// this file declares the system required for declaring lexicons as class object templates and
// instantiating them as objects of the class type

RunOncePath("BaseClass").

////
declare global class is Lexicon().

// internal list of classes
set class["registered"] to Lexicon().

// internal function. dont use. adds classObject <lexicon> to "registered" class list with the given className.
// it's supposed to be called via class:declare, 
set class["register"] to {
	declare parameter className.
	declare parameter classTemplate.
	class["registered"]:add(className,classTemplate).
}.

// registers and returns a new empty lexicon.
// the newly created lexicon is returned for a different file to declare functions and variables inside.
// those are stored for later use when instantiating a class object via class:instantiate or class:new 
set class["declare"] to {
	declare parameter className.
	
	set superclass to BaseClass.
	set newClass to superclass:copy().
	set newClass["type"] to className.
	
	//set newClass["superclassesList"] to superclass:superclassesList:copy().
	//newClass:superclassesList:add(superclass).
	set newClass["functionList"] to List().
	
	set newClass["function"] to BaseClass:function@:bind(newClass).
	set newClass["new"] to BaseClass:new@:bind(newClass).
	
	//set newClass["super"] to {
	//	declare parameter this.
	//	return this:superclassesList[this:superclassesList:LENGTH-1].
	//}.
	
	//set newClass["super"] to newClass:super:bind(newClass).
	
	class:register(className,newClass).
	
	return newClass.
}.

//set class["extend"] to {
//	declare parameter superClassName.
//	declare parameter className.
//	Print "a.".
//	wait 1.
//	set superclass to class:registered[superClassName].
//	Print "b.".
//	wait 1.
//	set newClass to superclass:copy().
//	Print "c.".
//	wait 1.
//	set newClass["type"] to className.
//	Print "d.".
//	wait 1.
//	
//	set newClass["superclassesList"] to superclass:superclassesList:copy().
//	Print "e.".
//	wait 1.
//	newClass:superclassesList:add(superclass).
//	Print "f.".
//	wait 1.
//	set newClass["functionList"] to List().
//	Print "g.".
//	wait 1.
//	
//	set newClass["function"] to BaseClass:function:bind(newClass).
//	Print "h.".
//	wait 1.
//	set newClass["new"] to BaseClass:new:bind(newClass).
//	Print "i.".
//	wait 1.
//	
//	set newClass["super"] to {
//		declare parameter this.
//		return this:superclassesList[this:superclassesList:LENGTH-1].
//	}.
//	Print "j.".
//	wait 1.
//	
//	set newClass["super"] to newClass:super:bind(newClass).
//	Print "k.".
//	wait 1.
//	
//	class:register(className,newClass).
//	Print "l.".
//	wait 1.
//	
//	return newClass.
//	
//	
//}.

set class["instantiate"] to {
	declare parameter className.
	
	set classTemplate to class:registered[className].
	
	set instantiatedObject to classTemplate:copy().
	
	//FOR superclass IN instantiatedObject:superclassesList { 
	//	FOR funcName IN superclass:functionList { 
	//		set instantiatedObject[funcName] to superclass[funcName]:bind(instantiatedObject).
	//	}
	//}
	FOR funcName IN classTemplate:functionList { 
		set instantiatedObject[funcName] to classTemplate[funcName]@:bind(instantiatedObject).
	}
	
	instantiatedObject:remove("function").
	instantiatedObject:remove("new").
	
	
	set funcId to instantiatedObject:functionList:find("constructor").
	
	if not funcId = -1 {
		instantiatedObject:constructor().
	}
	
	return instantiatedObject.
	//FOR VAR IN LEXICON:KEYS
	
}.

set class["new"] to {
	declare parameter className.
	return class:instantiate(className).
}.




