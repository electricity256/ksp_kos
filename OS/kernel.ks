

print "== kernel initializing ==".
declare global exit to false.
declare global encounteredError to false.

declare global logf to "None".
declare global logln to "None".

set logf to { // meant to be overridable
	declare parameter text.
	print text.
}.

set logln to { // meant to be overridable
	declare parameter text.
	print text.
}.

declare function ScrW {
	return terminal:width.
}

declare function systemError {
	declare parameter errorText.
	print "SYSTEM ERROR: "+errorText.
	set encounteredError to true.
}

declare function altitude {
	set tmpAlt to ship:altitude.
	if tmpAlt<10000 {
		return ALT:RADAR.
	}
	return tmpAlt.
}


declare function clamp {
	declare parameter value.
	declare parameter min.
	declare parameter max.
	if value<min {
		return min.
	}
	if value>max {
		return max.
	}
	return value.
}

declare function exp {
	declare parameter x.
	return constant:E^x.
}
/// ship firmware shit
//<--

  declare function printDebugInfo {
    logln("ship mass: "+ship:mass).
    logln("ship altitude: "+altitude()).
    logln("ship velocity: "+ship:velocity:surface).
  }

  declare function activateTaggedEngines {
    declare parameter name.
    set engineList to ship:partsTagged(name).
    for engine in engineList {
      engine:activate().
    }
  }

  declare function detachDetachable {
    declare parameter name.
    set towerList to ship:partsDubbed(name).
    for tower in towerList {
      tower:getmodule("LaunchClamp"):doaction("release clamp", true).
    }
  }

  declare function detachDetachableTagged {
    declare parameter name.
    set towerList to ship:partsTagged(name).
    for tower in towerList {
      tower:getmodule("LaunchClamp"):doaction("release clamp", true).
    }
  }

  declare function detachDecouplers {
    declare parameter name.
    declare parameter module.
    declare parameter action.
    set towerList to ship:partsDubbed(name).
    for tower in towerList {
      tower:getmodule(module):doaction(action, true).
    }
  }
  declare function detachDecouplersTagged {
    declare parameter name.
    declare parameter module.
    declare parameter action.
    set towerList to ship:partsTagged(name).
    for tower in towerList {
      tower:getmodule(module):doaction(action, true).
    }
  }

  declare global ang to R(0,0,0).

  declare function launchShip {
    logln("== launching ship ==").
    
	logf("! Turning off SAS and RCS...").
	RCS off.
	SAS off.
	logln("OK!").
    logf("1. Locking steering...").
    LOCK STEERING TO Up + ang.
    logln("OK!").
    logf("2. engines...").
    set ship:control:pilotMainThrottle to 1.
    activateTaggedEngines("engine1").
    logln("OK!").
    logf("3. detach launch towers..").
    detachDecouplersTagged("launchclamp","LaunchClamp","release clamp").
    logln("OK!").
    logf("! Ship has been launched").
  }

///-->


runoncepath("packageSystem").
PackageSystem:loadPackage("ClassSystem").
PackageSystem:loadPackage("ServiceSystem").
PackageSystem:loadPackage("Logger").
PackageSystem:loadPackage("MenuSystem").

//PackageSystem:loadPackage("ServiceSystem").
runoncepath("main").
