
////////// globals

	declare function altitude {
		set tmpAlt to ship:altitude.
		if tmpAlt<10000 {
			return ALT:RADAR.
		}
		return tmpAlt.
	}

  declare global exit to false.
  
  declare function clamp {
	  declare parameter value.
	  declare parameter min.
	  declare parameter max.
	  if value<min {
		  return min.
	  }
	  if value>max {
		  return max.
	  }
	  return value.
  }
  
////////////////////////////////////////// create logger class

	declare logger is Lexicon().

	set logger["list"] to List().
	logger:list:add("").

	set logger["identation"] to "  ".
	
	set logger["_log"] to {
		declare parameter text.
		set listid to (logger:list:LENGTH-1).
		set tmp1 to logger:list[listid].
		set tmp1 to tmp1+text.
		logger:list:remove(listid).
		logger:list:insert(listid,tmp1).
	}.
	
	set logger["logf"] to {
		declare parameter text.
		logger:_log(text).
		logger:onLogModified().
	}.

	set logger["logLn"] to {
		declare parameter text.
		logger:_log(text).
		logger:list:add("").
		logger:onLogModified().
	}.

	set logger["clearLog"] to {
		logger:list:clear().
		logger:list:add("").
		logger:onLogModified().
	}.
	
	set logger["printLastLines"] to {
		declare parameter count.
		set listlen to logger:list:length.
		if count > listlen {
			set count to listlen.
		}
		set startid to listlen-count.
		FROM {local k is startid.} UNTIL k >= listlen STEP {set k to k+1.} DO {
			PRINT logger:identation + (logger:list)[k].
		}
	}.
	
	set logger["onLogModified"] to {
		
	}.
  
	declare function logf {
		declare parameter text.
		logger:logf(text).
	}
	declare function logln {
		declare parameter text.
		logger:logln(text).
	}
	

////////////////////////////////////////// create menu class

  declare menu is Lexicon().

	

	set menu["option"] to 0. // the current option in the list

	set menu["optionsList"] to List(). // list of options
	set menu["menuWidth"] to 40.		// menu draw width
	set menu["logLines"] to 15.			// log list length
	
	
	set menu["programName"] to "PISS OS". // program name
	set menu["title"] to "menu title".	  // title under program name
	set menu["heading"] to "heading".     // heading under title
	
	// temp variables, recalculated only once on an event
	// used for stroring recalculated identation for fast access
	set menu["tmpProgramNameIdentation"] to "".
	set menu["tmpTitleIdentation"] to "".
	set menu["tmpHeadingIdentation"] to "".
	set menu["tmpSeparator"] to "".
	
	set menu["logLine"] to "== LOG ".
	set menu["tmpLogLine"] to "== LOG ".
	
	// gives menu the ability to redirect input for input through the menu	
	set menu["captureInput"] to false.
	set menu["captureInputBuffer"] to "".
	set menu["inputPromptText"] to "input". // text which is to be shown before the input
	// input chars are redirected to this function
	// when menu:captureInput is true
	// clears buffer and disables menu:captureInput if enter pressed
	set menu["menuInputEvent"] to {
		declare parameter char.
		//logln("char written "+char).
		if char = terminal:input:BACKSPACE {
			if menu:captureInputBuffer:length > 0 {
				set tmp to menu:captureInputBuffer:SUBSTRING(0, menu:captureInputBuffer:length-1).
				set menu:captureInputBuffer to tmp.
			}
			menu:draw().
			return.
		}
		if char = terminal:input:ENTER {
			set outp to menu:captureInputBuffer.
			set menu:captureInputBuffer to "".
			set menu:captureInput to false.
			menu:menuInputSubmitted(outp).
			return.
		}
		if char = terminal:input:ENDCURSOR { // escape todo
			set outp to menu:captureInputBuffer.
			set menu:captureInputBuffer to "".
			set menu:captureInput to false.
			menu:menuInputCancelled(outp).
			return.
		}
		set menu:captureInputBuffer to menu:captureInputBuffer+char.
		menu:draw().
		return.
	}.
	// overridable event - this event is called when
	// user presses enter while menu:captureInput is true.
	// submittedText contains the buffer
	set menu["menuInputSubmitted"] to {
		declare parameter submittedText.
	}.
	
	// overridable event - runs when input cancelled
	set menu["menuInputCancelled"] to {
		declare parameter submittedText.
	}.
	
	// static function, repeats /identChar/ /count/ amount of times
	set menu["staticRepeatChar"] to {
		
		declare parameter count.
		declare parameter identChar.
		
		set leftIdentStr to "".
		FROM {local k is 0.} UNTIL k >= count STEP {set k to k+1.} DO {
			set leftIdentStr to leftIdentStr+identChar.
		}
		
		return leftIdentStr.
	}.
	
	// static function, calculates how much to ident text from the left
	// given text size and screen width
	// and returns a string of chars to use as prefix
	set menu["staticCalcIdentation"] to {
		declare parameter textLength.
		declare parameter screenWidth.
		declare parameter identChar.
		
		set leftIdent to 0.
		if screenWidth>textLength {
			set leftIdent to (screenWidth-textLength)/2.
		}
		
		return menu:staticRepeatChar(leftIdent,identChar).
	}.
	
	// function which recalculates identations only when needed
	// and stores them in variables for fast use
	set menu["recalcTemp"] to {
		set menu:tmpSeparator to menu:staticRepeatChar(menu:menuWidth,"=").
		set menu:tmpProgramNameIdentation to menu:staticCalcIdentation(menu:programName:LENGTH,menu:menuWidth," ").
		set menu:tmpTitleIdentation to menu:staticCalcIdentation(menu:title:LENGTH,menu:menuWidth," ").
		set menu:tmpHeadingIdentation to menu:staticCalcIdentation(menu:heading:LENGTH,menu:menuWidth," ").
		set menu:tmpLogLine to menu:logLine+menu:staticRepeatChar(clamp(menu:menuWidth-menu:logLine:length,0,menu:menuWidth),"=").
	}.

	// adds option in the menu
	set menu["addOption"] to {
		declare parameter text.
		menu:optionsList:add(text).
	}.

	// clears all options from the menu
	set menu["clearOptions"] to {
		menu:optionsList:clear().
	}.
	
	// prints the header above the menu
	set menu["printHeader"] to {
		PRINT menu:tmpSeparator.
		PRINT menu:tmpProgramNameIdentation+menu:programName.
		PRINT menu:tmpTitleIdentation+menu:title.
		PRINT menu:tmpHeadingIdentation+menu:heading.
		PRINT menu:tmpSeparator.
	}.

	// prints all menu options onto the screen, and prefixes
	// the current option with a ">" character
	set menu["printOptions"] to {
		from {local k is 0.} until k >= menu:optionsList:length step {set k to k+1.} do {
			local selectionText is " ".
			if menu:option = k {
				set selectionText to ">".
			}
			print selectionText+k+". "+menu:optionsList[k].
		}
	}.

	// clears the screen and draws the whole menu
	set menu["draw"] to {
		if menu:captureInput {
			menu:drawInputPrompt().
		}else{
			clearscreen.
			menu:printHeader().
			PRINT " ".
			menu:printOptions().
			PRINT " ".
			PRINT menu:tmpLogLine.
			logger:printLastLines(menu:logLines).
			PRINT menu:tmpSeparator.
		}
	}.
	
	// clears the screen and draws the whole menu like a text prompt
	set menu["drawInputPrompt"] to {
		clearscreen.
		menu:printHeader().
		PRINT "Press END to cancel input.".
		PRINT " ".
		PRINT menu:inputPromptText+": "+menu:captureInputBuffer.
		PRINT " ".
		PRINT menu:tmpLogLine.
		logger:printLastLines(menu:logLines).
		PRINT menu:tmpSeparator.
	}.
	
	set menu["prompt"] to {
		declare parameter promptText.
		set menu:captureInputBuffer to "".
		set menu:captureInput 		to true.
		set menu:inputPromptText 	to promptText. // text which is to be shown before the input
	}.
	
	// overridable event for custom response based on the
	// option number that has been passed in as a parameter
	set menu["onSelected"] to {
		declare parameter option.
	}.

	// moves the current option to the next option in the list of options
	set menu["nextOption"] to {
		if menu:option < menu:optionsList:length-1 {
			set menu:option to menu:option+1.
		}else{
			set menu:option to 0.
		}
		menu:draw().
	}.
	
	// moves the current option to the previous option in the list of options
	set menu["previousOption"] to {
		if menu:option > 0 {
			set menu:option to menu:option-1.
		}else{
			set menu:option to menu:optionsList:length-1.
		}
		menu:draw().
	}.
	
	// selects the current option in the list, thereby also triggering the onSelected event
	set menu["selectOption"] to {
		menu:onSelected(menu:option).
		menu:draw().
	}.
	
	// sets the title and recalculates idents
	set menu["setTitle"] to {
		declare parameter text.
		set menu["title"] to text.
		menu:recalcTemp().
		menu:draw().
	}.
	
	// sets the heading and recalculates idents
	set menu["setHeading"] to {
		declare parameter text.
		set menu["heading"] to text.
		menu:recalcTemp().
		menu:draw().
	}.
	
	set menu["menuStack"] to List().
	
	// will execute previous menu function if added to stack
	// for convenience
	set menu["previousMenu"] to {
		set len to menu:menuStack:length.
		if len > 1 {
			menu:menuStack:remove(len-1).
			set func to menu:menuStack[len-2].
			menu:menuStack:remove(len-2).
			func().
		}
	}.
	
	set menu["addMenu"] to {
		declare parameter menufunc.
		menu:menuStack:add(menufunc).
	}.
	
	
	
	// not part of menu, event which will repaint screen when log modified
	set logger["onLogModified"] to {
		menu:draw().
	}.

////////// declare events



//  ON AG1 {
//    menu:previousOption().
//    return true.
//  }
//
//  ON AG2 {
//    menu:selectOption().
//    return true.
//  }
//
//  ON AG3 {
//    menu:nextOption().
//    return true.
//  }


WHEN terminal:input:haschar() THEN {
	if menu:captureInput {
		menu:menuInputEvent(terminal:input:getchar()).
	}else{ // standard controls
		set ch to terminal:input:getchar().
		if ch = terminal:input:DOWNCURSORONE {
			menu:nextOption().
		}
		if ch = terminal:input:UPCURSORONE {
			menu:previousOption().
		}
		if ch = terminal:input:RETURN {
			menu:selectOption().
		}
	}
	return true.
}



////////// declare functions

  declare function printDebugInfo {
    print "ship mass: "+ship:mass.
    print "ship altitude: "+altitude().
    print "ship velocity: "+ship:velocity:surface.
  }

  declare function activateTaggedEngines {
    declare parameter name.
    set engineList to ship:partsTagged(name).
    for engine in engineList {
      engine:activate().
    }
  }

  declare function detachDetachable {
    declare parameter name.
    set towerList to ship:partsDubbed(name).
    for tower in towerList {
      tower:getmodule("LaunchClamp"):doaction("release clamp", true).
    }
  }

  declare function detachDetachableTagged {
    declare parameter name.
    set towerList to ship:partsTagged(name).
    for tower in towerList {
      tower:getmodule("LaunchClamp"):doaction("release clamp", true).
    }
  }

  declare function detachDecouplers {
    declare parameter name.
    declare parameter module.
    declare parameter action.
    set towerList to ship:partsDubbed(name).
    for tower in towerList {
      tower:getmodule(module):doaction(action, true).
    }
  }
  declare function detachDecouplersTagged {
    declare parameter name.
    declare parameter module.
    declare parameter action.
    set towerList to ship:partsTagged(name).
    for tower in towerList {
      tower:getmodule(module):doaction(action, true).
    }
  }

  declare global ang to R(0,0,0).

  declare function launchShip {
    logln("== launching ship ==").
    
	logf("! Turning off SAS and RCS...").
	RCS off.
	SAS off.
	logln("OK!").
    logf("1. Locking steering...").
    LOCK STEERING TO Up + ang.
    logln("OK!").
    logf("2. engines...").
    set ship:control:pilotMainThrottle to 1.
    activateTaggedEngines("engine1").
    logln("OK!").
    logf("3. detach launch towers..").
    detachDecouplersTagged("launchclamp","LaunchClamp","release clamp").
    logln("OK!").
    logf("! Ship has been launched").
  }

// n back
// h forward
// i top
// k bottom
// j left
// l right
////////// main stuff runs here


declare function modeAutomaticMenu {
	
  set menu:option to 0.
  set menu:title to "Auto Orbit Seq".
  set menu:heading to "pending...".
  
  menu:clearOptions().
  menu:recalcTemp().
  
  menu:addOption("Stop & Back").
  
	menu:addMenu(modeAutomaticMenu@).
  
  set menu["onSelected"] to {
    declare parameter option.
    if option = 0 {
		menu:previousMenu().
    }
  }.
  
  launchShip().
  
  set timerMode to 0.
  set decoupleStage to 0.
  set interval to 1.0.
  
  SET futureTime to TIME:SECONDS + interval.
  WHEN TIME:SECONDS > futureTime THEN {
    SET futureTime to TIME:SECONDS + interval.
    
    ////////////////////////////////////////////////////////////////////////////////////
    if decoupleStage = 0 {
      set tanks to ship:partsTagged("attachedtank").
      for tank in tanks {
        if tank:Resources[0]:amount <= 0 {
          logf("! De-coupling empty booster...").
          tank:parent:getmodule("ModuleAnchoredDecoupler"):doaction("decouple", true).
          logln("OK!").
        }
      }
      if tanks:length = 0 {
        logln("! All boosters de-coupled.").
        set decoupleStage to 1.
      }
    }
    
    if decoupleStage = 1 {
	  set tmp to ship:partsTagged("bottomtank").
	  if tmp:length = 0 {
        logln("! WARNING - did not find ship's bottom tanks...").
		set decoupleStage to 2.
		return true.
	  }
      set bottomtank to tmp[0].
      if bottomtank:Resources[0]:amount <=0 {
		
		set tmp to ship:partsTagged("decoupler2").
		if tmp:length = 0 {
			logln("! WARNING - did not find ship's 2nd decoupler...").
			set decoupleStage to 2.
			return true.
		}
        set decoupler to tmp[0].
		
		set tmp to ship:partsTagged("engine2").
		if tmp:length = 0 {
			logln("! WARNING - did not find ship's 2nd engine...").
			set decoupleStage to 2.
			return true.
		}
        set engine2 to tmp[0].
		
        logf("! De-coupling empty fuel tank...").
        decoupler:getmodule("ModuleDecouple"):doaction("decouple", true).
        logln("OK!").
        logf("! Activating second engine...").
        engine2:activate().
        logln("OK!").
        set decoupleStage to 2.
      }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    if timerMode = 0 {                                  // launch sequence
		menu:setHeading("Launching...").
		logln("! Starting steering sequence").
		set timerMode to 1.
		return true.
    }
    
    if timerMode = 1 {                                  // steering sequence
      set height to altitude().
      if height<20000 {
        set ang to R(0,0,0).
      }
      if height>20000 and height<40000 {
        set calcumalation to (height-20000.0)/20000.0.
        if calcumalation > 1.0 {
          set calcumalation to 1.0.
        }
        set ang to R(0,-arcsin(calcumalation),0).
      }
      if height>40000 and height<70000 {
        set ang to R(0,-90,0).
      }
      if height>70000 {
        logln("! Steering Sequence finished.").
		logln("! Preparing to expand periapsis...").
        set ship:control:pilotMainThrottle to 0.
		menu:setHeading("Idle until apoapsis").
        set timerMode to 2.
      }
      LOCK STEERING TO Up + ang.
      return true.
    }
    
    if timerMode = 2 {                                // half orbit reached
      if ETA:Apoapsis < 8 {                           // wait to reach apoapsis
        logf("! Expanding periapsis to match apoapsis...").
        set ship:control:pilotMainThrottle to 1.
		set interval to 0.08.
		menu:setHeading("Expanding periapsis...").
        set timerMode to 3.
      }
      return true.
    }


    if timerMode = 3 {                                // expand periapsis
      if (alt:periapsis + 1000) > altitude() {      // check if periapsis reached apoapsis
        set ship:control:pilotMainThrottle to 0.
        logln("OK!").
		set interval to 1.0.
        set timerMode to 4.
      } 
      return true.
    }
    
    if timerMode = 4 {
      logf("! Unlocking steering...").
      unlock steering.
      logln("OK!").
      logf("! Turning on SAS and RCS...").
      RCS on.
      SAS on.
      logln("OK!").
      logln("= sequence completed =").
	  menu:setHeading("Finished").
      return false.
    }
    return true.
  }
  
  menu:draw().

}

declare function modeManualMenu {
	set menu:option to 0.
	set menu:title to "Manual Control".
	set menu:heading to "pending...".

	menu:clearOptions().
	menu:recalcTemp().

	menu:addOption("Launch").
	menu:addOption("Detach Boosters").
	menu:addOption("Decouple 1").
	menu:addOption("Shit.").
	menu:addOption("Debug info").
	menu:addOption("Back").

	menu:addMenu(modeManualMenu@).

	set menu["onSelected"] to {
		declare parameter option.
		if option = 0 {
			launchShip().
		}
		if option = 1 {
			detachDecouplers("Hydraulic Detachment Manifold","ModuleAnchoredDecoupler","decouple").
		}
		if option = 2 {
			detachDecouplersTagged("decoupler1","ModuleDecouple","decouple").
			activateTaggedEngines("engine2").
		}
		if option = 3 {
			detachDecouplersTagged("parachute","ModuleParachute","deploy chute").
		}
		if option = 4 {
			print "here is debug info: ".
			printDebugInfo().
		}
		if option = 5 {
			menu:previousMenu().
		}
	}.

	menu:draw().
}

set autoOrbitExpandAlt to 0.
set autoOrbitExpandCheckpoint to "apoapsis". // "apoapsis" or "nextnode" or "now"

declare function modeAutomaticExpandOrbitMenu {
	set menu:option to 0.
	set menu:title to "Auto Orbit Expand".
	set menu:heading to "configuration".

	menu:clearOptions().
	menu:recalcTemp().

	menu:addOption("Set altitude (cur: "+autoOrbitExpandAlt+")").
	menu:addOption("Set checkpoint (cur: "+autoOrbitExpandCheckpoint+")").
	menu:addOption("Start sequence").
	menu:addOption("Back").

	menu:addMenu(modeAutomaticExpandOrbitMenu@).

	set menu["onSelected"] to {
		declare parameter option.
		if option = 0 {
			modeAutomaticExpandOrbit_SetAltitude().
		}
		if option = 1 {
			modeAutomaticExpandOrbit_SetCheckpoint().
		}
		if option = 2 {
			modeAutomaticExpandOrbitMenu_Started().
		}
		if option = 3 {
			menu:previousMenu().
		}

	}.

	menu:draw().

}


declare function modeAutomaticExpandOrbit_SetAltitude {
	
	set menu:title to "Auto Orbit Expand".
	set menu:heading to "set target altitude".
  
	menu:clearOptions().
	menu:recalcTemp().
  
	menu:prompt("target altitude").

	menu:addMenu(modeAutomaticExpandOrbit_SetAltitude@).
	
	set menu["menuInputSubmitted"] to {
		declare parameter submittedText.
		set val to submittedText:toNumber(-1).
		if val = -1 {
			logln("Value "+submittedText+" is not a number.").
			logln("Target altitude was not modified.").
			menu:previousMenu().
			return.
		}
		if val <= 0 {
			logln("Specified value must be greater than 0").
			logln("Target altitude was not modified.").
			menu:previousMenu().
			return.
		}
		set autoOrbitExpandAlt to val.
		//logln("You entered input: "+submittedText).
		menu:previousMenu().
	}.
	
	set menu["menuInputCancelled"] to {
		declare parameter submittedText.
		logln("Target altitude was not modified.").
		menu:previousMenu().
	}.
	
	menu:draw().
}


declare function modeAutomaticExpandOrbit_SetCheckpoint {
	
	set menu:title to "Auto Orbit Expand".
	set menu:heading to "set starting checkpoint".
  
	menu:clearOptions().
	menu:recalcTemp().

	menu:addOption("Apoapsis").
	menu:addOption("Next Node").
	menu:addOption("Current Position").
	menu:addOption("Back").
	

	menu:addMenu(modeAutomaticExpandOrbit_SetCheckpoint@).
	
	set menu["onSelected"] to {
		declare parameter option.
		if option = 0 {
			set autoOrbitExpandCheckpoint to "apoapsis".
			menu:previousMenu().
		}
		if option = 1 {
			set autoOrbitExpandCheckpoint to "nextnode".
			menu:previousMenu().
		}
		if option = 2 {
			set autoOrbitExpandCheckpoint to "now".
			menu:previousMenu().
		}
		if option = 3 {
			menu:previousMenu().
		}

	}.
	
	menu:draw().

}

set modeAutomaticExpandOrbitMenu_Started_running to false.

declare function modeAutomaticExpandOrbitMenu_Started {
	set menu:option to 0.
	set menu:title to "Auto Orbit Expand".
	set menu:heading to "starting...".

	menu:clearOptions().
	menu:recalcTemp().

	menu:addOption("Stop & Back").

	menu:addMenu(modeAutomaticExpandOrbitMenu_Started@).
	
	
	set additionalTimeFromNow to 0.
	
	if autoOrbitExpandCheckpoint = "apoapsis" {
		set additionalTimeFromNow to ETA:Apoapsis-10.
	}
	if autoOrbitExpandCheckpoint = "nextnode" {
		if HasNode {
			set additionalTimeFromNow to nextnode:ETA-10.
		}else{
			logln("Error: you've not set a node").
			menu:previousMenu().
		}
	}
	if autoOrbitExpandCheckpoint = "now" {
		set additionalTimeFromNow to 10.
	}
	
	

	set timerMode to 0.
	set interval to 1.0.
	set targetStartTime to additionalTimeFromNow+TIME:SECONDS.
	
	logf("! Turning off SAS and RCS...").
	RCS off.
	SAS off.
	logln("OK!").
	
	set modeAutomaticExpandOrbitMenu_Started_running to true.
  
	SET futureTime to TIME:SECONDS + interval.
	WHEN TIME:SECONDS > futureTime THEN {
		SET futureTime to TIME:SECONDS + interval.
		
		lock steering to ship:velocity:orbit.
		
		if timerMode = 0 {
			menu:setHeading("Idle until target "+(targetStartTime-TIME:SECONDS)+"s").
			if TIME:SECONDS > targetStartTime {
				logf("! Expanding periapsis to match apoapsis...").
				menu:setHeading("Expanding periapsis...").
				set interval to 0.008.
				set ship:control:pilotMainThrottle to 1.
				set timerMode to 1.
			}
			return modeAutomaticExpandOrbitMenu_Started_running.
		}
		
		if timerMode = 1 {
			// (alt:periapsis + 1000) > altitude()
			if ALT:APOAPSIS > autoOrbitExpandAlt {
				set interval to 1.
				set ship:control:pilotMainThrottle to 0.
				logln("OK!").
				
				set targetStartTime to TIME:SECONDS+ETA:APOAPSIS-10.
				logf("! Waiting to reach apoapsis...").
				menu:setHeading("Idle until apoapsis").
				set timerMode to 2.
			}
			return modeAutomaticExpandOrbitMenu_Started_running.
		}
		
		if timerMode = 2 {
			menu:setHeading("Idle until target "+(targetStartTime-TIME:SECONDS)+"s").
			if TIME:SECONDS > targetStartTime {
				logln("OK!").
				
				set interval to 0.008.
				set ship:control:pilotMainThrottle to 1.
				logf("! Expanding periapsis to match target alt...").
				set timerMode to 3.
			}
			return modeAutomaticExpandOrbitMenu_Started_running.
		}
		
		if timerMode = 3 {
			if (alt:periapsis + 1000) > altitude() {
				set interval to 1.
				set ship:control:pilotMainThrottle to 0.
				logln("OK!").
				
				logf("! Unlocking steering...").
				unlock steering.
				logln("OK!").
				
					
				logf("! Turning on SAS and RCS...").
				RCS on.
				SAS on.
				logln("OK!").

				set modeAutomaticExpandOrbitMenu_Started_running to false.
				logln("= sequence completed =").
				menu:setHeading("Finished").
			}
			return modeAutomaticExpandOrbitMenu_Started_running.
		}
		
		return modeAutomaticExpandOrbitMenu_Started_running.
	}
	


	set menu["onSelected"] to {
		declare parameter option.
		if option = 0 {
			menu:previousMenu().
		}
	}.

	menu:draw().

}



declare function modeMainMenu {
	set menu:option to 0.
	set menu:title to "Main Menu".
	set menu:heading to "idle".
	
	menu:clearOptions().
	menu:recalcTemp().

	menu:addOption("Auto orbit").
	menu:addOption("Auto expand orbit").
	menu:addOption("Auto shrink orbit").
	menu:addOption("Manual tasks").
	menu:addOption("Tests").
	menu:addOption("Exit").

	menu:addMenu(modeMainMenu@).

	set menu["onSelected"] to {
		declare parameter option.
		if option = 0 {
			modeAutomaticMenu().
		}
		if option = 1 {
			modeAutomaticExpandOrbitMenu().
		}
		if option = 2 {
			modeAutomaticShrinkOrbitMenu().
		}
		if option = 3 {
			modeManualMenu().
		}
		if option = 4 {
			modeMenuTest().
		}
		if option = 5 {
			clearscreen.
			set exit to true.
			reboot.
		}
	}.
	
	menu:draw().
}


declare function modeMenuTest {
	set menu:option to 0.
	set menu:title to "Test Menu".
	set menu:heading to "idle".

	menu:clearOptions().
	menu:recalcTemp().

	menu:addOption("Test prompt").
	menu:addOption("Back").

	menu:addMenu(modeMenuTest@).

	set menu["onSelected"] to {
		declare parameter option.
		if option = 0 {
			modeMenuPromptTest().
		}
		if option = 1 {
			menu:previousMenu().
		}
	}.
	
	menu:draw().
}

declare function modeMenuPromptTest {
	set menu:title to "Prompt Menu".
	set menu:heading to "test".
  
	menu:clearOptions().
	menu:recalcTemp().
  
	menu:prompt("enter input").

	menu:addMenu(modeMenuPromptTest@).
	
	set menu["menuInputSubmitted"] to {
		declare parameter submittedText.
		logln("You entered input: "+submittedText).
		menu:previousMenu().
	}.
	
	set menu["menuInputCancelled"] to {
		declare parameter submittedText.
		logln("You've cancelled the prompt").
		menu:previousMenu().
	}.
	
	menu:draw().
}






modeMainMenu().



wait until exit.

/// todo
// 1. figure out classes [done]
// 2. stuff in menu added as modules [semi-done]
// 3. split code in different files [done]
// 4. auto detatch separate task
// 5. modeAutomaticShrinkOrbitMenu
//